import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        estructuras();
    }

    public static void ejercicio1(){
        int numero = 2;
        int resultado = (numero%2 == 0) ? numero*2 : numero*3;
        System.out.println(resultado);
    }

    public static void ejercicio2(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Por favor ingrese un entero: ");
        int num = leer.nextInt();
        int resultado1 = num*2;
        int resultado2 = num*3;
        System.out.println(resultado1);
        System.out.println(resultado2);
        leer.close();
    }

    public static void ejercicio3(){
        int gradoscelcius = 300;
        int gradosFarh = 32 + (9 * gradoscelcius/5);
        System.out.println(gradosFarh);
    }

    public static void ejercicio4(){
        int entero = 23;
        if(entero%2 == 0){
            System.out.println( entero +" es par");
        }else{
            System.out.println(entero + " es impar");
        }
    }

    public static void estructuras(){
        int var_1 = 10;
        int var_2 = 20;
        //condicionales
        if(var_1 < var_2){
            System.out.println("Es verdad");
        }else if(var_2 < var_1){
            System.out.println("Es falso");
        }

        System.out.println("--------------WHILE-----------------");
        int cont = 0;
        while(cont < 5){
            System.out.println(cont);
            ++cont;
            //int suma = 5 + ++cont;
        }
        cont = 0;
        System.out.println("--------------DO-WHILE-----------------");
        do{
            System.out.println(cont);
            cont +=1;
        }while(cont<5);

        System.out.println("--------------FOR-----------------");
        for(int i = 0 ; i < 10 ; i++){
            System.out.println(i);
        }

        //int contador = 0;
        //++contador;
        //contador ++;
    }

    //Método
    public static void saludar(){
        System.out.println("Hola mundo!");
    }
}
