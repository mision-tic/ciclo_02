import java.util.List;
import java.util.Scanner;


public class App {
    public static void main(String[] args) throws Exception {
        ejer4();
        
    }

    public static void ejercicio1(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingresa el día de tu nacimiento: ");// 06/09/1998
        int dia = leer.nextInt();
        System.out.print("Ingresa el mes de tu nacimiento: ");
        int mes = leer.nextInt();
        System.out.print("Ingresa el año de tu nacimiento: ");
        int año = leer.nextInt();
        int suma = dia + mes + año;
        int len = Integer.toString(suma).length();
        int t = 0;
        for(int i = 0; i < len ; i++){
            String num = String.valueOf(Integer.toString(suma).charAt(i));
            t += Integer.parseInt(num);
        }
        System.out.println("Tu número de la suerte es: " + t);  
        leer.close();
    }

    public static void ejemplolista(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingresa el día de tu nacimiento: ");
        String fecha = leer.nextLine();
        int nume = 25;
        System.out.println(fecha);
        leer.close();
    }

    public static void ejercicio2(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Por favor ingrese el valor por unidad del producto: ");
        int valor_unidad = leer.nextInt();
        System.out.print("Por favor ingrese el número de productos vendidos: ");
        int pro_vendidos = leer.nextInt();
        System.out.print("Por favor ingrese el porcentaje del IVA: ");
        int por_iva = leer.nextInt();
        int precio_total = valor_unidad*pro_vendidos;
        int precio_venta = precio_total + precio_total*1/por_iva;
        System.out.print("El precio de venta es: " + precio_venta);
        leer.close();
    }
    public static void ejercicio3(){
        Scanner leer = new Scanner(System.in);
        System.out.print(" Ingresa un número de varias cifras: ");
        int N = leer.nextInt();
        System.out.print(" Ingresa el número de cifras que deseas quitarle al número anterior: ");
        int n = leer.nextInt();
        String cadena = String.valueOf(N);
        int longitud = cadena.length();
        String resultado = cadena.substring(0,longitud - n);
        System.out.println(resultado);
        leer.close();
    }

    public static void ejercicio4(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Por favor ingrese la velocidad: ");
        int velocidad = leer.nextInt();
        int resultado = velocidad*5/18;
        System.out.println(resultado);
        leer.close();
    }

    public static void ejercicio5(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Por favor ingrese el valor de un cateto: ");
        int cateto1 = leer.nextInt();
        System.out.print("Por favor ingrese el valor del otro cateto: ");
        int cateto2 = leer.nextInt();
        double hipotenusa = Math.sqrt(Math.pow(cateto1,2) + Math.pow(cateto2,2));
        System.out.println(hipotenusa);
        leer.close();
    }

    public static void ejercicio6() {
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa un número: ");
        int numero = leer.nextInt();
        if(numero%10==0){
            System.out.println("El número que ingresaste es múltiplo de 10.");
        }else{
            System.out.println("El número que ingresaste no es múltiplo de 10.");
        }
        leer.close();
    }

    public static void ejercicio7(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingresa un caracter: ");
        char caracter = leer.next().charAt(0);
        char caracter_M = Character.toUpperCase(caracter);
        if(caracter == caracter_M ){
            System.out.print("El caracter es una letra mayúscula.");
        }else{
            System.out.print("El caracter es una letra minúscula.");
        }
        leer.close();
    }

    public static void ejercicio8(){
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa el numerador de la división: ");
        int numerador = leer.nextInt();
        System.out.println("Ingresa el denominador de la división: ");
        int denominador = leer.nextInt();
        if(denominador != 0){
            double div = numerador/denominador;
            System.out.println(div); 
        }else{
            System.out.println("Debes digitar un valor distinto de cero para el denominador.");
        }
        leer.close();
        
    } //PREGUNTAR LO DE 8/10=0


    public static void ejercicio9(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingresa el primer número: ");
        int pn = leer.nextInt();
        System.out.print("Ingresa el segundo número: ");
        int sn = leer.nextInt();
        System.out.print("Ingresa el tercer número: ");
        int tn = leer.nextInt();
        if (pn > sn & pn > tn){
            System.out.println("El número mayor es: " + pn);
        }else if(sn > pn & sn > tn){
            System.out.println("El número mayor es: " + sn);
        }else if(tn > pn & tn > sn){
            System.out.println("El número mayor es: " + tn);
        }
        leer.close();
    }

    public static void ejercicio10(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingresa el valor de la hora: ");
        int hora = leer.nextInt();
        System.out.print("Ingresa el svalor de los minutos: ");
        int minutos = leer.nextInt();
        System.out.print("Ingresa el valor de los segundos: ");
        int segundos = leer.nextInt();
        if((-1 < hora & hora < 25) && (-1< minutos & minutos < 61) && (-1 < segundos & segundos < 61)){
            System.out.println("La hora que ingresaste es válida.");
        }else{
            System.out.println("La hora que ingresaste no es válida.");
        }
        leer.close();       
    }

    public static void ejercicio11(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingrese un mes: ");
        int mes = leer.nextInt(); 
        if((0 < mes) && (mes > 13)){
            System.out.println("El valor del mes debe estar entre 1 y 12.");
        }else if((mes == 4) || (mes == 6) || (mes == 9) || (mes == 11)){
            if(mes == 4){
                System.out.println("El mes que ingresaste corresponde a abril y este mes tiene 30 días.");
            }
            if(mes == 6){
                System.out.println("El mes que ingresaste corresponde a junio y este mes tiene 30 días.");
            }
            if(mes == 9){
                System.out.println("El mes que ingresaste corresponde a septiembre y este mes tiene 30 días.");
            }
            if(mes == 11){
                System.out.println("El mes que ingresaste corresponde a noviembre y este mes tiene 30 días.");
            }
        }else if((mes == 1) || (mes == 3) || (mes == 5) || (mes == 7) || (mes == 8) || (mes == 10) || (mes == 12)){
            if(mes == 1){
                System.out.println("El mes que ingresaste corresponde a enero y este mes tiene 31 días."); 
            }
            if(mes == 3){
                System.out.println("El mes que ingresaste corresponde a marzo y este mes tiene 31 días.");
            }
            if(mes == 5){
                System.out.println("El mes que ingresaste corresponde a mayo y este mes tiene 31 días.");
            }
            if(mes == 7){
                System.out.println("El mes que ingresaste corresponde a julio y este mes tiene 31 días.");
            }
            if(mes == 8){
                System.out.println("El mes que ingresaste corresponde a agosto y este mes tiene 31 días.");
            }
            if(mes == 10){
                System.out.println("El mes que ingresaste corresponde a octubre y este mes tiene 31 días.");
            }
            if(mes == 12){
                System.out.println("El mes que ingresaste corresponde a diciembre y este mes tiene 31 días.");
            }
        }
        else if(mes == 2){
            System.out.println("El mes que ingresaste corresponde a febrero y este mes tiene 28 días.");
        }
        leer.close();
    }

    public static void ejercicio11_1(){
        int[] dias = {31,28,31,30,31,30,31,31,30,31,30,31};
        String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        for(int i = 0 ; i < 12; i++){
            System.out.println("El mes que ingresaste corresponde a " + meses[i] + " y este mes tiene " + dias[i]+ " días.");
        }
    }

    public static void ejercicio12(){
        int cont = 0;
        while((cont < 101) && (cont % 3 == 0)){
            System.out.println(cont);
            ++cont;
        }
        
    }

    public static void ejercicio12_1(){
        for(int i = 0 ; (i < 101) & (i % 3 == 0); i++){
            System.out.println(i);
        }   
    }
 
    public static void ejercicio12_2(){
        int cont = 0;
        do{
            System.out.println(cont);
            cont +=1;
        }while((cont < 101) && (cont % 3 == 0));
    }


    public static void ejercicio13(){
        Scanner leer = new Scanner(System.in);
        System.out.print("Ingresa el valor en pesos que quieres pasar a dolar: ");
        int pesos = leer.nextInt();
        int dolar = pesos/3700;
        System.out.println(dolar);
    }

    // Mostrar los N primeros términos de la serie de Fibonacci
    public static void ejercicio14(){
        
    }
    

    public static void ejercicio15(){
        
    }


    public static void ejercicio16(){
        
    }


    public static void ejercicio17(){
        
    }

    //Realizar la suma, la resta, la división y la multiplicación de dos números
    //leídos por teclado y mostrar en pantalla “La <operación> de <número 1>
    //y <número 2> es igual a <resultado> ”

    public static void ejer1(){
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa el primer valor para operar: ");
        int valor1 = leer.nextInt();
        System.out.println("Ingresa el segundo valor para operar: ");
        int valor2 = leer.nextInt();
        System.out.println("----------SUMA----------");
        int suma = valor1 + valor2;
        System.out.println("La suma de " + valor1 + " y " + valor2 + " es igual a "+ suma);
        System.out.println("----------RESTA----------");
        int resta = valor1 - valor2;
        System.out.println("La resta de " + valor1 + " y " + valor2 + " es igual a "+ resta);
        System.out.println("----------MULTIPLICACIÓN----------");
        int mul = valor1 * valor2;
        System.out.println("La multiplicación de " + valor1 + " y " + valor2 + " es igual a "+ mul);
        System.out.println("----------DIVISIÓN----------");
        if(valor2 != 0){
            double div = valor1/valor2;
            System.out.println("La división de " + valor1 + " y " + valor2 + " es igual a "+ div);
        }else{
            System.out.println("No se puede realizar una división por cero.");
        }
        leer.close();
    }

    //Realizar un programa que realice el promedio de las notas de un alumno,
    //para ello el programa va a tener que solicitar el nombre del alumno y las
    //notas de las 3 evaluaciones. Si el alumno tiene un promedio mayor o igual a
    //3.0 también debe imprimir “Aprobado”, si no alcanzó esa nota debe imprimir
    //“Reprobado”
    public static void ejer2(){
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa el nombre del estudiante: ");
        String nombre = leer.next();
        System.out.println("Ingresa la primera nota: ");
        double n1 = leer.nextDouble();
        System.out.println("Ingresa la segunda nota: ");
        double n2 = leer.nextDouble();
        System.out.println("Ingresa la tercera nota: ");
        double n3 = leer.nextDouble();
        double promedio = (n1+n2+n3)/3;
        if(promedio >= 3){
            System.out.println("El alumno " + nombre + " aprobó la materia con una nota de " + promedio);
        }else{
            System.out.println("El alumno " + nombre + " reprobó la materia con una nota de " + promedio);
        }
        leer.close();
    }

    //Realizar un programa que calcule el sueldo de un trabajador, el programa
    //solicita el número de horas que has trabajado en un mes, las horas se
    //pagan a $30.000.

    public static void ejer3(){
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa el número de horas del trabajador: ");
        int horas = leer.nextInt();
        double sueldo = horas * 30000;
        System.out.println("El sueldo correspondiente es de: " + sueldo);
        leer.close();
    }

    //Solicitar un número al usuario y mostrar la tabla de multiplicar de ese
    //número, desde el 0 hasta el 10. Truco: Usa un bucle for para recorrer la
    //tabla y mostrar los datos.
    public static void ejer4(){
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa un número: ");
        int num = leer.nextInt();
        System.out.println("La tabla de multiplicar del número " + num + " es: ");
        for(int i = 0 ; i <= 10 ; i++){
            System.out.println(i + " x " + num + " = " + i*num);
        }
        leer.close();
    }

    //Generar un número aleatorio entre el 1 y el 100, el usuario lo tiene que
    //adivinar introduciendo el número por teclado. En el caso que el número a
    //adivinar sea mayor al ingresado, decirle al usuario “El número que busca es
    //mayor”, de lo contrario, “El número que busca es menor”. El programa
    //finalizará cuando se introduzca el número correcto. Nota: usar la clase
    //Random para generar el número aleatorio.
    public static void ejer5(){
        
    }



    // PARA ACTUALIZAR
    // Git add .
    // Git commit -m "comentario"
    // Git push origin master

    

}
