import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        // comentar una línea de código
        /*
        comentar 
        varias lineas
        o un bloque 
        de código
        */
        /**
         * Esto es un comentario
         * de documentación
         */

        
        System.out.println("Holiwis!");

        //DECLARAR UNA VARIABLE
        //PYTHON -> numero: int = 0
        int numero = 0;
        double decimal = 4.5;
        float flotante = (float)2.2; //castear
        boolean bandera = true;
        String mensaje = "Buen día";
        char caracter = 'c';
        String[] arreglo;
        int [][] matriz;
        //mensaje.equals("Juan");

        /**
         * Solicitar datos por consola
         */
        // CRear un objeto scanner
        try(Scanner leer = new Scanner(System.in)) {
            System.out.print("Por favor ingresa un entero: ");
        //Capturar un entero
        int num = leer.nextInt(); //Equivalente al input
        System.out.println("Digitaste: " + num);
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Debe de digitar un entero!");
        }
        // Cerrar
        //leer.close();







    }
}
