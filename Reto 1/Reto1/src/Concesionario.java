 /**
 * Autor: Lexly Vanessa Sosa Jerez
 * Fecha: Julio 2021
 * Empresa: Mision Tic
 * Descripción: Solución reto 1
 */

public class Concesionario{
    /*********************
     * Atributos
    *********************/
    public int tiempo ;
    public double capital;
    public double interes;

    /*****************
     * Constructor
    ******************/
    
    public Concesionario(){
        tiempo = 0;
        capital = 0.0;
        interes = 0.0;

    }
    

    public Concesionario(int _tiempo, double _capital, double _interes){
        this.tiempo = _tiempo;
        this.capital = _capital;
        this.interes = _interes;
    }

    /*********************
     * Métodos
    *********************/
    
    public float calcular_interes_simple(int _tiempo, double _capital, double _interes){
        return (float)(_capital * _interes * _tiempo);
    }

    public float calcular_interes_compuesto(int _tiempo, double _capital, double _interes){
        return (float)(_capital*(Math.pow(1 + _interes,_tiempo)-1));
    }

    public String compararInvConcesionario(int _tiempo, double _capital, double _interes){
        if(_tiempo <= 0 || _capital <= 0 || _interes <= 0){
            return ("Faltan datos para calcular la diferencia en el total de intereses generados para el proyecto.");
        }else{
            float resultado = calcular_interes_compuesto(_tiempo, _capital, _interes)-calcular_interes_simple(_tiempo, _capital, _interes);
            return ("La diferencia en el total de intereses generados para el proyecto, si escogemos entre evaluarlo a una tasa de interés Compuesto y evaluarlo a una tasa de interés Simple, asciende a la cifra de: $ " + resultado);
        }
    }

}
// $1126114.052065204
//1126114.052065204