import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class App {
    public static void main(String[] args) throws Exception {
        //construir_carros("./carros.txt");
        cargar_carros("./carros.txt");
    }

    public static void construir_carros(String file){
        //Construcción de los objetos tipo carro
        Carro objCarro_1 = new Carro("BBS546", "Beige", "Toyota", 5000);
        Carro objCarro_2 = new Carro("BBS544", "Azul", "Tesla", 4000);
        try {
            //Construimos la referencia de salida de datos
            FileOutputStream fichero = new FileOutputStream(file);

            //Manejo de stream con objetos
            ObjectOutputStream objStream = new ObjectOutputStream(fichero);

            //Envío de datos/objetos por el flujo de datos objSistema
            objStream.writeObject(objCarro_1);
            objStream.writeObject(objCarro_2);

            //Cerramos el sistema

            objStream.close();

        } catch (Exception e) {
            //TODO: handle exception
            System.err.println("Error al guardar los datos");
        }
    }


    public static void cargar_carros(String file){
        try {
            //Coonstruir referencia del fichero como entrada de datos
            FileInputStream fichero = new FileInputStream(file);
            //construcción del canal de flujo de datos para la entrada de objetos
            ObjectInputStream objStream = new ObjectInputStream(fichero);
            while(true){
                Carro objCarro = (Carro) objStream.readObject();
                System.out.println("--------Carro -------");
                System.out.println("Placa" + objCarro.getPlaca());
                System.out.println("Color" + objCarro.getColor());
                System.out.println("Fabricante" + objCarro.getFabricante());
                System.out.println("Caballos de fuerza" + objCarro.getCaballos_fuerza());
        }
        

            /*
            //Cargar objetos por medio del flujo de datos 
            Carro objCarro_1 = (Carro) objStream.readObject();
            Carro objCarro_2 = (Carro) objStream.readObject();
            //mostrar información
            System.out.println("--------Carro 1-------");
            System.out.println("Placa"+objCarro_1.getPlaca());
            System.out.println("Color"+objCarro_1.getColor());
            System.out.println("Fabricante"+objCarro_1.getFabricante());
            System.out.println("Caballos de fuerza"+objCarro_1.getCaballos_fuerza());
            System.out.println("--------Carro 2-------");
            System.out.println("Placa"+objCarro_2.getPlaca());
            System.out.println("Color"+objCarro_2.getColor());
            System.out.println("Fabricante"+objCarro_2.getFabricante());
            System.out.println("Caballos de fuerza"+objCarro_2.getCaballos_fuerza());
            objStream.close();
            */
        }catch(FileNotFoundException ef){
            System.err.println("No se encontró el fichero referenciado");
        }catch(IOException e){
            //TODO: handle exception
            System.err.println("Error en el flujo de datos");
        }catch(Exception e){
            System.err.println("Error general del sistema");
        }
        
    }
}
