public class Urbano extends Flete {
    // Constantes
    public final static int TIEMPO = 2;
    // Constructores

    public Urbano(double peso, double tamanio) {
        super(peso, tamanio);
    }

    public Urbano(double precioBase) {
        super(precioBase);
    }

    public Urbano() {
        super();
    }

    // Metodos

    public double calcularPrecio() {
        //double precioFinal = precioBase + ( peso * tamanio * TIEMPO);
        //return precioFinal;
        return super.getPrecioBase() + (super.getPeso() * super.getTamanio() * TIEMPO);
    }

}
