
public class PrecioTotal {
    //Atributos
    private double totalPrecios ;
    private double totalNacional ;
    private double totalUrbano ;
    private Flete[] flete; 
    //Constructores
    public PrecioTotal(Flete[] flete) {
        this.flete = flete;
        
    }
    //Métodos
    public void calcularTotales() {
        for(int i=0; i < flete.length ; i++){
            totalPrecios += flete[i].calcularPrecio();
            if(flete[i].getClass() == Nacional.class){
                totalNacional += flete[i].calcularPrecio();
            }else{
                totalUrbano += flete[i].calcularPrecio();
            }
        }  
    }
  
    public void mostrarTotales(){
        //Calculo de totales
        //calcularTotales(totalUrbano, totalNacional, totalPrecios);
        calcularTotales();
        System.out.println("Total Fletes " + totalPrecios);
        System.out.println("Total Nacional " + totalNacional);
        System.out.println("Total Urbano " + totalUrbano);
    }

}
    
