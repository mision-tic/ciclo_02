public class Nacional extends Flete{
    // Constantes
    public static final double CAPACIDAD= 8.0;
    
    // Constructores  
    public Nacional(double peso, double tamanio) {
        super(peso, tamanio);
    }

    public Nacional(double precioBase) {
        super(precioBase);
    }

    public Nacional() {
        super();
    }

    // Metodos
    //calcularPrecioNacional = precioBase + (peso * tamanio * CAPACIDAD).
    public double calcularPrecio(){
        //return get.precioBase + ( peso * tamanio * CAPACIDAD);
        //return getPrecioBase + (getPeso * getTamanio *CAPACIDAD);
        return super.getPrecioBase() + (super.getPeso() * super.getTamanio() * CAPACIDAD);
       
    }
}