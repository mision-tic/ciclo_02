package model.dao;

//Estructura de datos
import java.util.ArrayList;

import model.vo.Lider;
import model.vo.Proyecto;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//Clase para conexión
import util.JDBCUtilities;

public class ProyectoDao {

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {

        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        Connection conexion = JDBCUtilities.getConnection();

        try {
            String query = "SELECT DISTINCT Constructora, Serial FROM Proyecto WHERE Clasificacion = ? ";

            // Preparar el query
            PreparedStatement pStatement = conexion.prepareStatement(query);
            pStatement.setString(1, "Casa");
            // Ejecutar la consulta
            ResultSet resultado = pStatement.executeQuery();

            if (resultado.next()) {
                Proyecto requerimiento1 = new query_requerimiento_1();
                requerimiento1.setNombre_constructora(resultado.getString("Constructora"));
                requerimiento1.setSerial(resultado.getString("Serial"));
            }
            conexion.close();
            resultado.close();
            pStatement.close();

        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e);
        }

        return requerimiento1;

    }

    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {

    }// Fin del método query_requerimiento_2

    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {

    }// Fin del método query_requerimiento_3

    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException {

    }// Fin del método query_requerimiento_4

}