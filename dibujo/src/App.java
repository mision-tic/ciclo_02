import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;

public class App {
    public static void main(String[] args) throws Exception {
        crear_lienzo();
    }
    public static void crear_lienzo(){
        //Crear ventana (JFrame)
        JFrame objFrame = new JFrame("Software de dibujo");
        //Crear objeto Dibujo
        Dibujo objDibujo = new Dibujo();
        //Añadir elementos al frame
        objFrame.add(objDibujo, BorderLayout.CENTER);
        JLabel lblMensaje = new JLabel("Click sostenido para dibujar");
        objFrame.add(lblMensaje, BorderLayout.NORTH);
        //Configuracion de la ventana (objFrame)
        objFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        objFrame.setSize(400,200);
        objFrame.setVisible(true);

    }
}
