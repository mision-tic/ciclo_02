import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Dibujo extends JPanel {
    /***
     * Atributos
    */
    private Point[] puntos;
    private int contar_puntos;

    /**
     * Constructor
    */
    public Dibujo(){
        //Inicializar los atributos
        this.puntos = new Point[10000];
        this.contar_puntos = 0;
        //Poner a la escucha para eventos en el mouse
        this.addMouseMotionListener(new MouseMotionAdapter(){
            //Escucha el evento del mouse en un click sostenido
            public void mouseDragged(MouseEvent evt){
                if(contar_puntos < puntos.length){
                    puntos[contar_puntos] = evt.getPoint();
                    contar_puntos++;
                    //Refesca el panel
                    repaint();
                
                }
                
            }
        });
    }
    //Sobreescribir el método de la clase padre JPanel

    public void paintComponent(Graphics graphic){
        super.paintComponent(graphic);
        //dibujar los puntos del arreglo puntos
        for(int i=0; i< contar_puntos; i++){
            graphic.fillOval(this.puntos[i].x, this.puntos[i].y, 4, 4);
        }
    }
    
}
