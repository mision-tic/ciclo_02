import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class App {
    public static void main(String[] args) throws Exception {
        
        String texto = "";
        // Objeto de entrada de datos

        BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
        // Objeto de salida de datos
        PrintWriter salida = new PrintWriter(System.out, true);
        // salida de datos
        salida.println("Por favor ingrese un texto: ");

        try{
            // captura de datos
            texto = entrada.readLine();
        }catch (Exception e) {
            System.err.println();

        }
        salida.println("El texto escrito es: " + texto);
        
    }
}
