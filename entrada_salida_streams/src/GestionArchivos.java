import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.StringTokenizer;

public class GestionArchivos {

    public static void main(String[] args) {
        // crear_fichero("./numeros.txt");
        lectura_fichero("./numeros.txt");
    }

    // Salida de datos
    public static void crear_fichero(String file) {
        // int[][] numeros = new int[5][5];
        int[][] numeros = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };
        try (PrintWriter salida = new PrintWriter(file)) {
            // Objeto de salida de datos
            // PrintWriter salida = new PrintWriter("./numeros.txt");
            // Recorrido de la matriz
            for (int i = 0; i < numeros.length; i++) {// Recorre cada fila
                for (int j = 0; j < numeros[i].length; j++) {// Recorer las columnas
                    // salida.print(numeros[i][j]+"\t");
                    salida.printf("%d \t", numeros[i][j]);
                }
                salida.println("");
            }
            // Cerrar la conexión del flujo de datos
            // salida.close();
        } catch (IOException e) {
            // TODO: handle exception
            System.err.println("Error en la salida de datos");
        }
    }

    // Lectura de datos / entrada de datos
    public static void lectura_fichero(String file) {

        try {
            File archivo = new File(file);
            if (archivo.exists()) {
                Scanner entrada = new Scanner(archivo);
                System.out.println("Números del fichero: ");
                while(entrada.hasNext()){
                    StringTokenizer numeros = new StringTokenizer( entrada.next(), "\t" );
                    while(numeros.hasMoreTokens()){
                        System.out.print(numeros.nextToken() + "\t");
                    }
                    System.out.println("");
                }
                entrada.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

}

