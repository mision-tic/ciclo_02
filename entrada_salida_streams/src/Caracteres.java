public class Caracteres {
    public static void main(String[] args) throws Exception {
        int numBytes = 0;
        char caracter;
        System.out.println("Por favor escriba un texto: ");
        try {
            do{
                caracter = (char) System.in.read();
                System.out.println(caracter);
                numBytes++;  
            }while( caracter != '\n' );
            System.out.println("Número de Bytes leídos: "+numBytes);
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println("Error");
        }
        
    }
}
