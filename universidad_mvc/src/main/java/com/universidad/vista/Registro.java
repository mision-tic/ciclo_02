package com.universidad.vista;

import javax.swing.JOptionPane;

import com.universidad.controlador.UniversidadControlador;
import com.universidad.modelo.Universidad;

public class Registro {
    private UniversidadControlador cUniversidad;

    public Registro(){
        this.cUniversidad = new UniversidadControlador();
        this.menu();
    }

    public void menu(){
        String info_menu = "-----Ingrese una opción-----\n";
        info_menu += "1 -> Crear universidad \n";
        info_menu += "2 -> Mostrar universidades \n";
        info_menu += "0 -> Salir \n";

        int opcion = Integer.parseInt(JOptionPane.showInputDialog(null, info_menu));

        switch(opcion){
            case 1:
                this.solicitar_datos();
                break;
            case 2:
                this.mostrar_universidades();
                break;
            case 0:
                System.exit(0);


        }
    }

    public void solicitar_datos(){
        String nombre=JOptionPane.showInputDialog(null, "Por favor ingrese el nombre de la universidad ");
        String direccion=JOptionPane.showInputDialog(null, "Ingrese la dirección de la universidad "+nombre);
        String nit=JOptionPane.showInputDialog(null, "Ingrese el nit de la universidad "+nombre);
        //mostrar objeto con los datos capturados
        this.cUniversidad.crear_universidad(nombre, direccion, nit);
        //ventana informativa
        JOptionPane.showMessageDialog(null, "¡Universidad creada con éxito!");
        this.menu();

        //this.mostrar_datos_universidad(0);
    }

    public void mostrar_universidades(){
        for(int i = 0; i < this.cUniversidad.getArrayUniversidad().size(); i++){
            this.mostrar_datos_universidad(i);
        }
        this.menu();
    }

    public void mostrar_datos_universidad(int pos){
        Universidad objUniversidad = this.cUniversidad.getUniversidad(pos);
        String info = "-------"+objUniversidad.getNombre()+"-------\n";
        info += "Dirección: "+objUniversidad.getDireccion()+"\n";
        info += "Nit: "+objUniversidad.getNit()+"\n";
        JOptionPane.showMessageDialog(null, info);
    }

    
}
