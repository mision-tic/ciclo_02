package com.utp.p76.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.utp.p76.controlador.Controlador;
import com.utp.p76.modelo.vo.Lider;

import java.awt.*;
import java.awt.event.*;


public class VistaFormulario extends JFrame{
    //Atributos
    //label -> Mostrar texto
    private JLabel lblDocumento;
    private JLabel lblNombre;
    private JLabel lblPrimerApellido;
    private JLabel lblSegunndoApellido;
    private JLabel lblSalario;
    private JLabel lblCargo;
    private JLabel lblCiudadResidencia;
    private JLabel lblClasificacion;
    private JLabel lblFechaNacimiento;

 
    //Campos de texto
    private JTextField txtDocumento;
    private JTextField txtNombre;
    private JTextField txtPrimerApellido;
    private JTextField txtSegundoApellido;
    private JTextField txtSalario;
    private JTextField txtCargo;
    private JTextField txtCiudadResidencia;
    private JTextField txtClasificacion;
    private JTextField txtFechaNacimiento;

    //Botones
    private JButton btnBuscar;
    private JButton btnCrear;
    private JButton btnActualizar;
    //private JButton btnEliminar

    //Controlador

    private Controlador objControlador;


    //Constructor

    public VistaFormulario(){
        this.objControlador = new Controlador();
        //Titulo de la ventana
        this.setTitle("Formulario Lideres");
        //Ubicacion y tamaño
        this.setBounds(250, 250, 600, 300);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        //Construir objeto layout
        GridLayout gLayout = new GridLayout(5, 1, 9, 9);
        //Contenedor de elementos
        Container objContainer = new Container();
        //Indicar el layout al objContainer
        objContainer.setLayout(gLayout);

        BorderLayout bLayout = new BorderLayout(15,15);
        Container contentPane = this.getContentPane();
        contentPane.setLayout(bLayout);


        //Inicializar los elementos gráficos y añadirlos al esquema

        this.lblNombre = new JLabel("Nombre: ");
        objContainer.add(this.lblNombre);

        this.txtNombre = new JTextField();
        objContainer.add(this.txtNombre);
        
        this.lblPrimerApellido = new JLabel("Primer Apellido: ");
        objContainer.add(this.lblPrimerApellido);

        this.txtPrimerApellido = new JTextField();
        objContainer.add(this.txtPrimerApellido);

        this.lblSegunndoApellido = new JLabel("Segundo apellido: ");
        objContainer.add(this.lblSegunndoApellido);

        this.txtSegundoApellido = new JTextField();
        objContainer.add(this.txtSegundoApellido);

        this.lblSalario = new JLabel("Salario: ");
        objContainer.add(this.lblSalario);

        this.txtSalario = new JTextField();
        objContainer.add(this.txtSalario);

        this.lblCargo = new JLabel("Cargo: ");
        objContainer.add(this.lblCargo);

        this.txtCargo = new JTextField();
        objContainer.add(this.txtCargo);

        this.lblCiudadResidencia = new JLabel("Ciudad de Residencia: ");
        objContainer.add(this.lblCiudadResidencia);

        this.txtCiudadResidencia = new JTextField();
        objContainer.add(this.txtCiudadResidencia);

        this.lblClasificacion = new JLabel("Clasificación: ");
        objContainer.add(this.lblClasificacion);

        this.txtClasificacion = new JTextField();
        objContainer.add(this.txtClasificacion);

        this.lblFechaNacimiento = new JLabel("Fecha de nacimiento: ");
        objContainer.add(this.lblFechaNacimiento);

        this.txtFechaNacimiento = new JTextField();
        objContainer.add(this.txtFechaNacimiento);


        GridLayout northGridLayout = new GridLayout(1, 1, 5, 5);
        Container northContainer = new Container();
        northContainer.setLayout(northGridLayout);

        this.lblDocumento = new JLabel("Documento identidad: ");
        northContainer.add(this.lblDocumento);

        this.txtDocumento = new JTextField();
        northContainer.add(this.txtDocumento);

        this.btnBuscar = new JButton("Buscar");
        northContainer.add(this.add(btnBuscar));
        
        objContainer.add(new JLabel());
        this.btnCrear = new JButton("Crear Lider");
        objContainer.add(this.btnCrear);

        objContainer.add(new JLabel());
        this.btnActualizar = new JButton("Actualizar Datos");
        objContainer.add(this.btnActualizar);

        //Manejador de eventos

        this.btnBuscar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                String documento = txtDocumento.getText();
                Lider objLider = objControlador.buscar_lider(documento);
                //Poner la informacion en los campos de texto
                txtNombre.setText(objLider.getNombre());
                txtPrimerApellido.setText(objLider.getPrimer_apellido());
                txtSegundoApellido.setText(objLider.getSegundo_apellido());
                txtSalario.setText(""+objLider.getSalario());
                txtCargo.setText(objLider.getCargo());
                txtCiudadResidencia.setText(objLider.getCiudad_residencia());
                txtClasificacion.setText(""+objLider.getClasificacion());
                txtFechaNacimiento.setText(objLider.getFecha_nacimiento());
            }
        });

        this.add(objContainer, BorderLayout.CENTER);
        this.add(northContainer, BorderLayout.NORTH);
    }



    
}
