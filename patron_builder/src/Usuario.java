public class Usuario{
    /**
     * Atributos
     */

     private String nombre;
     private String apellido;
     private String direccion;
     private String telefono;
     private String email;


    /**
     * Constructores
     */
    public Usuario(){

    }

    public Usuario(UsuarioBuilder objBuilder){
        if(objBuilder.getNombre() == null || objBuilder.getApellido() == null
        || objBuilder.getDireccion() == null || objBuilder.getEmail() == null
        || objBuilder.getEmail() == null){
            throw new IllegalArgumentException("Los siguientes datos son requeridos: nombre, apellido, email, telefono, direccion.");
        }else{
            this.nombre = objBuilder.getNombre();
            this.apellido = objBuilder.getApellido();
            this.direccion = objBuilder.getDireccion();
            this.telefono = objBuilder.getTelefono();
            this.email = objBuilder.getEmail();
        }

    }

    /**
     * Acciones
     */

     public String toString(){
        String strUsuario = "------Usuario------\n";
        strUsuario += "Nombre: "+nombre+"\n";
        strUsuario += "Apellido: "+apellido+"\n";
        strUsuario += "Dirección: "+direccion+"\n";
        strUsuario += "Telefono: "+telefono+"\n";
        strUsuario += "Email: "+email+"\n";
        strUsuario += "------------------------------";
        return strUsuario;
     }
}