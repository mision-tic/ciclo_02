public class App {
    public static void main(String[] args) throws Exception {
        UsuarioBuilder objBuilder = new UsuarioBuilder();

        objBuilder = objBuilder.getNombre("Juan");
        objBuilder = objBuilder.getApellido("Hernandez");
        objBuilder = objBuilder.getTelefono("124466");
        objBuilder = objBuilder.getDireccion("kr 56");
        objBuilder = objBuilder.getEmail("her.jfidjf@.com");

        Usuario objUsuario_1 = objBuilder.builder();
        System.out.println(objUsuario_1);

        Usuario objUsuario_2 = new UsuarioBuilder().setNombre("Andrés").setApellido("Hernandez").setDireccion("cra 100").setTelefono("12344").setEmail("andres@gmail.com").builder();
        System.out.println(objUsuario_2);

    }
}
