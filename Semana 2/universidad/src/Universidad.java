/**
 * Autor: Lexly Vanessa Sosa Jerez
 * Fecha: 07 Julio 2021
 * Empresa: Mision Tic
 * Descripción: Proyecto que me representa una composición entre una universidad y una facultad
 */

public class Universidad{
    /*********************
     * Atributos
     *********************/
    private String nombre;
    private String nit;
    private Facultad[] facultades;
    private Carrera[] carreras;

    /*********************
     * Método constructor
     *********************/
    // Quien construye un objeto
    public Universidad(String nombre, String nit){
        this.nombre = nombre;
        this.nit = nit;
        // Se crea el arreglo reservando un espacio en memoria de 5 posiciones
        this.facultades = new Facultad[5];
        this.carreras = new Carrera[5];
    }

    /*************
     * Consultores
    ***************/
    public Facultad getFacultad(int pos){
        return this.facultades[pos];
    }

    public Carrera getCarrera(int pos){
        return this.carreras[pos];
    }

    /********************
    * Métodos o acciones
    ********************/

    public void crear_facultad(String nombre, int pos){
        //crear objeto facultad
        Facultad objFacultad = new Facultad(nombre, this);
        this.facultades[pos] = objFacultad;
        // Accede al objeto y obtiene el nombre de la facultad
        //String nombreF = objFacultad.getNombre();
        //System.out.println(nombreF);

    }

} 