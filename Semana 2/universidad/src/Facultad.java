/**
 * Autor: Lexly Vanessa Sosa Jerez
 * Fecha: 07 Julio 2021
 * Empresa: Mision Tic
 * Descripción: Clase que me representa una facultad
 */
public class Facultad {
    /************
     * Atributos
    ************/

    private String nombre;
    private Universidad universidad;

    /*************
     * Constructor
    *************/

    public Facultad(String nombre, Universidad universidad){
        this.nombre = nombre;
        this.universidad = universidad;

    }

    /*************
     * Consultores
     * (Getters)
    *************/

    public String getNombre(){
        return this.nombre;
    }

    /*************
     * Modificadores
     * (Setters)
    *************/

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void crear_carreras(String nombre){
        System.out.println("Creando carrera..." + nombre); 
    }


    
}
