/**
 * Autor: Lexly Vanessa Sosa Jerez
 * Fecha: 07 Julio 2021
 * Empresa: Mision Tic
 * Descripción: Clase que me representa una carrera
 */
public class Carrera {
    /************
     * Atributos
    ************/

    private String nombre;

    /*************
     * Constructor
    *************/

    public Carrera(String nombre, Facultad facultad){
        this.nombre = nombre;
    }
    
    /*************
     * Consultores
     * (Getters)
    *************/

    public String getNombre(){
        return this.nombre;
    }
    
}
