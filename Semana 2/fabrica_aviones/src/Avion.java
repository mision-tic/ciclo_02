/**
 * Autor:
 * Empresa:
 * Fecha:
 * Ciudad:
 * Descripción:
 */


public class Avion {
    /*****************
     * ATRIBUTOS
    ******************/
    private String color;
    private double tamanio;

    /*****************
     * CONSTRUCTOR
     * recibe como parámetro un color y un tamaño
    ******************/

    public Avion(String color, double tamanio){

    }

    /***************
     * METODOS
     * (acciones de la clase)
    ***************/

    public void aterrizar(){

        System.out.println("Aterrizando...");

    }

    public boolean despegar(){
        System.out.println("Despegando...");
        return true;
    }

    public void frenar(){
        System.out.println("Frenando...");
    }


}
