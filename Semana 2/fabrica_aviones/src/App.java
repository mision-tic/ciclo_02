public class App {
    public static void main(String[] args) throws Exception {
        //Construir avión de carga

        //AvionCarga objAvionCarga = new AvionCarga("Gris",150.4);

        //objAvionCarga.cargar();

        //System.out.println(objAvionCarga.despegar());

        //objAvionCarga.aterrizar();

        //objAvionCarga.descargar();

        // Construir avión de pasajeros

        //AvionPasajeros objAvionPasajeros = new AvionPasajeros("Azul",203.6, 78);

        //objAvionPasajeros.servir();

        // Construir avión militar

        //AvionMilitar objAvionMilitar = new AvionMilitar("Verde",209.7,3);

        //objAvionMilitar.disparar();
        
        //AvionMilitar objAvionMilitar = new AvionMilitar("azul", 123.5);
        //objAvionMilitar.setMsisiles(6);
        //objAvionMilitar.despegar();
        //objAvionMilitar.detectar_amenaza(false);
        //for(int i=0;i<8;i++){
        //    objAvionMilitar.detectar_amenaza(true);
        //}
        //crear objeto de tipo ingeniero
        Ingeniero objIngeniero = new Ingeniero();
        //Construir avion
        objIngeniero.construir_avion("verde", 65.7, 8);
        AvionMilitar objAvionMilitar = objIngeniero.getAvionMilitar(0);
        System.out.println("---Objeto construido---");
        System.out.println(objAvionMilitar);
        System.out.println("--Acceder al objeto avion---");
        objAvionMilitar.despegar();

    }
}
