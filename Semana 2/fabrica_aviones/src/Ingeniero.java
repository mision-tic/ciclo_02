import java.util.ArrayList;
import java.util.List;

public class Ingeniero {
    /**
     * Atributos
    */

    private String nombre;
    private String apellido;
    private String cedula;
    //private AvionMilitar[] aviones;
    private List<AvionMilitar> aviones;
    // Métodos

    public Ingeniero(){
        this.aviones = new ArrayList<AvionMilitar>();
        //this.aviones = new AvionMilitar[10];
    }

    //Consultor

    public AvionMilitar getAvionMilitar(int pos){
        return this.aviones.get(pos);
    }


    public boolean construir_avion(String color, double tamanio, int misiles){
        AvionMilitar objAvionMilitar = new AvionMilitar(color, tamanio , misiles);
        this.aviones.add(objAvionMilitar);
        return true;
    }

}
