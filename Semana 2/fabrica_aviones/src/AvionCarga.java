public class AvionCarga extends Avion{
    /*****************
     * CONSTRUCTOR
     * recibe como parámetro un color y un tamaño
    ******************/
    public AvionCarga(String color, double tamanio){
        super(color, tamanio);
    }

    /*****************
     * METODOS
     * (ACCIONES)
    ******************/
    public boolean cargar(){

        System.out.println("Cargando...");

    }

    public void descargar(){

        System.out.println("Descargando...");

    }
}
