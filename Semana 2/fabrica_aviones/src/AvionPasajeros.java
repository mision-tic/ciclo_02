public class AvionPasajeros extends Avion{
    
    /*****************
     * ATRIBUTOS
    ******************/
    private int pasajeros;

    /*****************
     * MODIFICADORES
    ******************/
    //public void setpasajeros(int pasajeros){
    //    this.pasajeros = pasajeros;
    //}

    /*****************
     * CONSTRUCTOR
     * recibe como parámetro un color y un tamaño
    ******************/
    public AvionPasajeros(String color, double tamanio, int pasajeros){
        super(color, tamanio);
    }

    /*****************
     * METODOS
     * (ACCIONES)
    ******************/

    public void servir(){
        System.out.println("Servir");
    }

   
    
}
